package com.jk.asmouse

import android.accessibilityservice.AccessibilityService
import android.accessibilityservice.GestureDescription
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Path
import android.graphics.PixelFormat
import android.util.Log
import android.view.*
import android.view.accessibility.AccessibilityEvent
import android.widget.ImageView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.coroutines.*

class AirCursor : AccessibilityService() {

    private var cursorView: View? = null
    private lateinit var cursorLayout: WindowManager.LayoutParams
    private var windowMgr: WindowManager? = null

    private var moveCursor : Boolean = false

    private var currentX: Float = 0f
    private var currentY: Float = 0f

    private var cursorWidthHalf = 0
    private var cursorHeightHalf = 0

    private var checkActivate: Boolean = false

    private val checkReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                ACTION_CHECK -> {
                    Log.w(TAG, "onReceive:ACTION_CHECK. [$context]")

                    if(checkActivate) {
                        LocalBroadcastManager.getInstance(this@AirCursor).sendBroadcast(Intent(this@AirCursor, MainActivity::class.java).also { intent ->
                            intent.action = ACTION_ACTIVE
                        })
                    } else {
                        LocalBroadcastManager.getInstance(this@AirCursor).sendBroadcast(Intent(this@AirCursor, MainActivity::class.java).also { intent ->
                            intent.action = ACTION_DEACTIVE
                        })
                    }

                }
            }
        }
    }
    private val stopReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                ACTION_STOP -> {
                    Log.w(TAG, "onReceive:ACTION_STOP. [$context]")
                    stopCursor()
                }
            }
        }
    }
    private val moveReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                ACTION_MOVE -> {
                    Log.w(TAG, "onReceive:ACTION_MOVE. [$context]")

                    currentX = intent.getFloatExtra("x", currentX)
                    currentY = intent.getFloatExtra("y", currentY)
                    move(currentX, currentY)
                }
            }
        }
    }
    private val clickReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                ACTION_CLICK -> {
                    Log.w(TAG, "onReceive:ACTION_CLICK. [$context]")
                    click()
                }
            }
        }
    }

    override fun onServiceConnected() {
        super.onServiceConnected()
        Log.w(TAG, "onServiceConnected.")

        checkActivate = true

        initCursor()

        LocalBroadcastManager.getInstance(this).registerReceiver(checkReceiver, IntentFilter(ACTION_CHECK))
        LocalBroadcastManager.getInstance(this).registerReceiver(stopReceiver, IntentFilter(ACTION_STOP))
        LocalBroadcastManager.getInstance(this).registerReceiver(moveReceiver, IntentFilter(ACTION_MOVE))
        LocalBroadcastManager.getInstance(this).registerReceiver(clickReceiver, IntentFilter(ACTION_CLICK))

        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, MainActivity::class.java).also { intent ->
            intent.action = ACTION_ACTIVE
        })
    }
    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        Log.w(TAG, "onAccessibilityEvent. [$event]")
    }
    override fun onInterrupt() {
        Log.w(TAG, "onInterrupt.")
    }
    override fun onUnbind(intent: Intent?): Boolean {
        Log.w(TAG, "onUnbind. [$intent]")
        stopCursor()
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        Log.w(TAG, "onDestroy.")
        stopCursor()
        super.onDestroy()
    }
    private fun initCursor() {
        cursorView = View.inflate(baseContext, R.layout.cursor_overlay, null)

        val cursorImage = cursorView?.findViewById<ImageView>(R.id.cursor)
        val viewTreeObserver = cursorImage?.viewTreeObserver
        viewTreeObserver?.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                cursorImage.viewTreeObserver?.removeOnPreDrawListener(this)
                cursorWidthHalf = cursorImage.measuredWidth.div(2) ?: 0
                cursorHeightHalf = cursorImage.measuredHeight.div(2) ?: 0
                return true
            }
        })

        cursorLayout = WindowManager.LayoutParams()
        cursorLayout.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        cursorLayout.format = PixelFormat.TRANSLUCENT
        cursorLayout.flags = WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        cursorLayout.width = WindowManager.LayoutParams.WRAP_CONTENT
        cursorLayout.height = WindowManager.LayoutParams.WRAP_CONTENT
        cursorLayout.gravity = Gravity.START or Gravity.TOP

        windowMgr = getSystemService(WINDOW_SERVICE) as WindowManager
        windowMgr?.addView(cursorView, cursorLayout)
    }

    private fun stopCursor() {
        Log.w(TAG, "stopCursor. [$checkActivate]")

        if(!checkActivate) { return }

        checkActivate = false

        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, MainActivity::class.java).also { intent ->
            intent.action = ACTION_DEACTIVE
        })

        LocalBroadcastManager.getInstance(this).unregisterReceiver(checkReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(stopReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(moveReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(clickReceiver)

        windowMgr?.removeView(cursorView)

        disableSelf()
    }

    private fun click() {
        val path: Path = Path()
        val clickX : Float = if (cursorLayout.x + cursorWidthHalf > 0) { (cursorLayout.x + cursorWidthHalf).toFloat() } else { 0f}
        val clickY : Float = if (cursorLayout.y + cursorHeightHalf > 0) { (cursorLayout.y + cursorHeightHalf).toFloat() } else { 0f}
        Log.w(TAG, "click. [${cursorLayout.x}, ${cursorLayout.y}] -> [$clickX, $clickY]")
        path.moveTo(clickX, clickY)

        val stroke = GestureDescription.StrokeDescription(path, 50, 100)
        val gestureBuilder = GestureDescription.Builder()
        gestureBuilder.addStroke(stroke)
        dispatchGesture(gestureBuilder.build(), object: GestureResultCallback() {
            override fun onCompleted(gestureDescription: GestureDescription?) {
                super.onCompleted(gestureDescription)
            }
        }, null)
    }

    private fun move(x: Float, y: Float) {
        cursorLayout.x = (x - cursorWidthHalf).toInt()
        cursorLayout.y = (y - cursorHeightHalf).toInt()
        moveCursor = true

        Log.w(TAG, "MoveTo [$x, $y] / ImageHalf [$cursorWidthHalf, $cursorHeightHalf] / Layout [${cursorLayout.x}, ${cursorLayout.y}]")

        try {
            GlobalScope.launch(Dispatchers.Main) {
                if(moveCursor) {
                    windowMgr?.updateViewLayout(cursorView, cursorLayout)
                    moveCursor = false
                }
            }
        } catch (ex : Exception) {
            Log.e(TAG, ex.message.toString())
        }
    }

    companion object {
        const val TAG = ">>>>> AirCursor >>>>>"

        const val ACTION_CHECK = "com.jk.asmouse.aircursor.check"
        const val ACTION_STOP = "com.jk.asmouse.aircursor.stop"

        const val ACTION_ACTIVE = "com.jk.asmouse.aircursor.active"
        const val ACTION_DEACTIVE = "com.jk.asmouse.aircursor.deactive"

        const val ACTION_MOVE = "com.jk.asmouse.aircursor.move"
        const val ACTION_CLICK = "com.jk.asmouse.aircursor.click"
    }
}