package com.jk.asmouse

import android.Manifest
import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.FrameLayout
import android.widget.ToggleButton
import androidx.appcompat.widget.SwitchCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.mediapipe.components.PermissionHelper
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private var pointerActive: Boolean = false
    private var switchPermission: SwitchCompat? = null

    private var toggleVisible: ToggleButton? = null

    private val pointerActiveReceiver = object :BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                AirCursor.ACTION_ACTIVE -> {
                    pointerActive = true
                    switchPermission?.isChecked = pointerActive
                }
            }
        }
    }
    private val pointerDeactivateReceiver = object :BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                AirCursor.ACTION_DEACTIVE -> {
                    pointerActive = false
                    switchPermission?.isChecked = pointerActive
                }
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.w(TAG, "onCreate. [$savedInstanceState]")

        val switchOverlay = findViewById<SwitchCompat>(R.id.swtOverlay)
        switchOverlay.setOnCheckedChangeListener { _, isChecked ->
            Log.w(TAG, "switchOverlay.setOnCheckedChangeListener. [$isChecked]")
            if(isChecked) {
                if(!isServiceRunning(this, AirMouse::class.java)) {
                    Log.w(TAG, "StartService : AirMouse")
                    startService(Intent(this, AirMouse::class.java))

                    if(toggleVisible?.isEnabled == true) {
                        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirMouse::class.java).also { intent ->
                                intent.action = AirMouse.ACTION_SHOW
                        })
                    } else {
                        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirMouse::class.java).also { intent ->
                            intent.action = AirMouse.ACTION_HIDE
                        })
                    }
                }
            } else {
                if(isServiceRunning(this, AirMouse::class.java)) {
                    Log.w(TAG, "StopService : AirMouse.")
                    stopService(Intent(this, AirMouse::class.java))
                }
            }
            toggleVisible?.isEnabled = isChecked
        }
        toggleVisible = findViewById<ToggleButton>(R.id.tglVisible)
        toggleVisible?.isChecked = true
        toggleVisible?.setOnCheckedChangeListener { _, isChecked ->
            Log.w(TAG, "toggleVisible.setOnCheckedChangeListener. [$isChecked]")
            if(isChecked) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirMouse::class.java).also { intent ->
                    intent.action = AirMouse.ACTION_SHOW
                })
            } else {
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirMouse::class.java).also { intent ->
                    intent.action = AirMouse.ACTION_HIDE
                })
            }
        }

        val checkPointer = findViewById<FrameLayout>(R.id.chkPointer)
        checkPointer.setOnClickListener {
            Log.w(TAG, "checkPointer : setOnClickListener. [$pointerActive]")
            if(pointerActive) {
                if(isServiceRunning(this, AirCursor::class.java)) {
                    Log.w(TAG, "StopService : AirCursor.")
                    LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
                        intent.action = AirCursor.ACTION_STOP
                    })
                }
            } else {
                if(!isServiceRunning(this, AirCursor::class.java)) {
                    Log.w(TAG, "StartService : AirCursor.")
                    startService(Intent(this, AirCursor::class.java))
                }

                startActivity(Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS))
                LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
                    intent.action = AirCursor.ACTION_CHECK
                })
            }
        }

        switchPermission = findViewById<SwitchCompat>(R.id.swtPointer)
        switchPermission?.isClickable = false

        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "requestPermissions(Manifest.permission.CAMERA).")
            requestPermissions(arrayOf(Manifest.permission.CAMERA), PERMISSION_REQUEST_CAMERA)
        }
        if(checkSelfPermission(Manifest.permission.SYSTEM_ALERT_WINDOW) != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "requestPermissions(Manifest.permission.SYSTEM_ALERT_WINDOW).")
            requestPermissions(arrayOf(Manifest.permission.SYSTEM_ALERT_WINDOW), PERMISSION_REQUEST_SYSTEM_ALERT_WINDOW)
        }
        if(!Settings.canDrawOverlays(this)) {
            Log.w(TAG, "Request : ACTION_MANAGE_OVERLAY_PERMISSION.")
            startActivityForResult(Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + applicationContext.packageName)), PERMISSION_REQUEST_SYSTEM_ALERT_WINDOW)
        }
        if(checkSelfPermission(Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
            Log.w(TAG, "requestPermissions(Manifest.permission.FOREGROUND_SERVICE).")
            requestPermissions(arrayOf(Manifest.permission.FOREGROUND_SERVICE), PERMISSION_REQUEST_FOREGROUND_SERVICE)
        }
    }

    override fun onResume() {
        super.onResume()
        Log.v(TAG, "onResume.")

        // Cursor Receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(pointerActiveReceiver, IntentFilter(AirCursor.ACTION_ACTIVE))
        LocalBroadcastManager.getInstance(this).registerReceiver(pointerDeactivateReceiver, IntentFilter(AirCursor.ACTION_DEACTIVE))

        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
            intent.action = AirCursor.ACTION_CHECK
        })
    }

    override fun onPause() {
        Log.v(TAG, "onPause.")

        // Cursor Receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pointerActiveReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pointerDeactivateReceiver)

        super.onPause()
    }

    override fun onDestroy() {
        Log.w(TAG, "onDestroy.")
        if(isServiceRunning(this, AirMouse::class.java)) {
            stopService(Intent(this, AirMouse::class.java))
        }
        if(isServiceRunning(this, AirCursor::class.java)) {
            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
                intent.action = AirCursor.ACTION_STOP
            })
        }

        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.v(TAG, "onRequestPermissionsResult. [$requestCode][$permissions][$grantResults]")

        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun isServiceRunning(context: Context, serviceClass: Class<*>) : Boolean {
        try {
            val mgr = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
            for(service in mgr!!.getRunningServices(Integer.MAX_VALUE)) {
                if(serviceClass.name == service.service.className) {
                    Log.w(TAG, "isServiceRunning. [${serviceClass.name}]")
                    return true
                }
            }
        } catch (e : Exception) {
            Log.e(TAG, "isServiceRunning. [${e.printStackTrace()}]")
            e.printStackTrace()
        }

        return false
    }

    companion object {
        const val TAG = ">>>>> MenuActivity >>>>>"

        const val PERMISSION_REQUEST_CAMERA = 19020
        const val PERMISSION_REQUEST_SYSTEM_ALERT_WINDOW = 19021
        const val PERMISSION_REQUEST_FOREGROUND_SERVICE = 19022
    }
}