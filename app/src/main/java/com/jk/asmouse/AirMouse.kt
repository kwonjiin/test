package com.jk.asmouse

import android.Manifest
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.PixelFormat
import android.graphics.SurfaceTexture
import android.os.IBinder
import android.util.Log
import android.util.Size
import android.view.*
import android.widget.FrameLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.mediapipe.components.*
import com.google.mediapipe.formats.proto.LandmarkProto
import com.google.mediapipe.framework.AndroidAssetUtil
import com.google.mediapipe.framework.Packet
import com.google.mediapipe.framework.PacketCallback
import com.google.mediapipe.framework.PacketGetter
import com.google.mediapipe.glutil.EglManager

class AirMouse : Service(), LifecycleOwner {

    // Mediapipe
    private var rootView : View? = null
    private var display : FrameLayout? = null
    private var windowMgr : WindowManager? = null

    private var surfaceTexture: SurfaceTexture? = null
    private var surfaceView: SurfaceView? = null
    private var eglMgr: EglManager? = null
    private var processor: FrameProcessor? = null

    private var converter: ExternalTextureConverter? = null
    private var cameraHelper: CameraXPreviewHelper? = null

    private var lifecycleRegistry: LifecycleRegistry? = null

    private var screenSize: Size = Size(0, 0)
    private var readyClick : Boolean = false

    private val showReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                ACTION_SHOW -> {
                    Log.w(TAG, "onReceive:ACTION_SHOW. [$context]")
                    display?.visibility = View.VISIBLE
                }
            }
        }
    }
    private val hideReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                ACTION_HIDE -> {
                    Log.w(TAG, "onReceive:ACTION_HIDE. [$context]")
                    display?.visibility = View.GONE
                }
            }
        }
    }


    init {
        // Load all native libraries needed by the app.
        System.loadLibrary("mediapipe_jni");
        System.loadLibrary("opencv_java3");
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.w(TAG, "onCreate.")

        lifecycleRegistry = LifecycleRegistry(this)
        lifecycleRegistry?.markState(Lifecycle.State.CREATED)

        val displayMetrics = resources.displayMetrics
        screenSize = Size(displayMetrics.widthPixels, displayMetrics.heightPixels)

        initOverlay()
        initDisplay()

        converter = ExternalTextureConverter(eglMgr?.context)
        converter?.setFlipY(true)
        converter?.setConsumer(processor)

        if(PermissionHelper.permissionsGranted(this, arrayOf(Manifest.permission.CAMERA))) {
            Log.w(TAG, "PermissionHelper.permissionsGranted. [Manifest.permission.CAMERA]")
            startCamera()
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(showReceiver, IntentFilter(ACTION_SHOW))
        LocalBroadcastManager.getInstance(this).registerReceiver(hideReceiver, IntentFilter(ACTION_HIDE))
    }

    override fun onDestroy() {
        Log.w(TAG, "onDestroy.")

        converter?.close()
        surfaceTexture?.release()

        windowMgr?.removeView(rootView)

        LocalBroadcastManager.getInstance(this).unregisterReceiver(showReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(hideReceiver)

        super.onDestroy()
    }

    override fun getLifecycle(): Lifecycle {
        Log.w(TAG, "getLifecycle. [$lifecycleRegistry]")
        return lifecycleRegistry!!
    }

    private fun initOverlay() {
        Log.w(TAG, "initOverlay.")

        val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        rootView = layoutInflater.inflate(R.layout.camera_overlay, null)

        val layoutParams = WindowManager.LayoutParams()
        layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        layoutParams.format = PixelFormat.TRANSLUCENT
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.gravity = Gravity.START or Gravity.TOP

        windowMgr = getSystemService(WINDOW_SERVICE) as WindowManager
        windowMgr?.addView(rootView, layoutParams)


        surfaceView = SurfaceView(this)
        surfaceView?.visibility = View.GONE
        surfaceView?.holder?.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                Log.w(TAG, "SurfaceHolder.Callback.surfaceCreated [$holder]")
                processor?.videoSurfaceOutput?.setSurface(holder.surface)
            }

            override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
                Log.w(TAG, "SurfaceHolder.Callback.surfaceChanged [$holder][$format][$width, $height]")
                val displaySize = cameraHelper?.computeDisplaySizeFromViewSize(Size(width, height))
                if(displaySize != null)
                {
                    Log.w(TAG, "SurfaceHolder.Callback.surfaceChanged.. [$displaySize]")
                    converter?.setSurfaceTextureAndAttachToGLContext(surfaceTexture, displaySize.height, displaySize.width)
                }
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                Log.w(TAG, "SurfaceHolder.Callback.surfaceDestroyed [$holder]")
                processor?.videoSurfaceOutput?.setSurface(null)
            }
        })


        display = rootView?.findViewById(R.id.overlay_display)
        display?.addView(surfaceView)

        val displayParams = display?.layoutParams
        displayParams?.width = (screenSize.width * 0.25).toInt()
        displayParams?.height = (screenSize.height * 0.25).toInt()
        display?.layoutParams = displayParams
    }

    private fun initDisplay() {
        Log.w(TAG, "initDisplay.")

        AndroidAssetUtil.initializeNativeAssetManager(this)

        eglMgr = EglManager(null)

        processor = FrameProcessor(this, eglMgr!!.nativeContext, BINARY_GRAPH_NAME, INPUT_STREAM_NAME, OUTPUT_STREAM_NAME)
        processor?.videoSurfaceOutput?.setFlipY(true)
        processor?.addPacketCallback(OUTPUT_LANDMARKS_STREAM_NAME, PacketCallback { packet ->
            Log.w(TAG, "OUTPUT_LANDMARKS_STREAM_NAME.PacketCallback [$packet]")

            val multiHandLandmarks = PacketGetter.getProtoVector(packet, LandmarkProto.NormalizedLandmarkList.parser())

            if(multiHandLandmarks.size <= 0) return@PacketCallback
            if(multiHandLandmarks[0].getLandmark(8) == null) return@PacketCallback

            val x = multiHandLandmarks[0].getLandmark(8).x * screenSize.width
            val y = multiHandLandmarks[0].getLandmark(8).y * screenSize.height
            Log.w(TAG, "Landmark [${multiHandLandmarks[0].getLandmark(8).x}, ${multiHandLandmarks[0].getLandmark(8).y}] / ScreenSize [${screenSize.width}, ${screenSize.height}] / MoveTo [$x, $y]")

            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
                intent.action = AirCursor.ACTION_MOVE
                intent.putExtra("x", x.toFloat())
                intent.putExtra("y", y.toFloat())
            })


            if(multiHandLandmarks[0].getLandmark(4).x < multiHandLandmarks[0].getLandmark(0).x) {
                // Right
                    if(readyClick) {
                        if(multiHandLandmarks[0].getLandmark(4).x < multiHandLandmarks[0].getLandmark(3).x) {
                            readyClick = false

                            LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
                                intent.action = AirCursor.ACTION_CLICK
                            })
                        }
                    } else {
                        if(multiHandLandmarks[0].getLandmark(4).x > multiHandLandmarks[0].getLandmark(2).x) {
                            readyClick = true
                        }
                    }
            } else {
                // Left
                if(readyClick) {
                    if(multiHandLandmarks[0].getLandmark(4).x > multiHandLandmarks[0].getLandmark(3).x) {
                        readyClick = false

                        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(this, AirCursor::class.java).also { intent ->
                            intent.action = AirCursor.ACTION_CLICK
                        })
                    }
                } else {
                    if(multiHandLandmarks[0].getLandmark(4).x < multiHandLandmarks[0].getLandmark(2).x) {
                        readyClick = true
                    }
                }
            }
        })

        val packetCreator = processor?.packetCreator
        val inputNumHands = HashMap<String, Packet>()
        inputNumHands[INPUT_NUM_HANDS_SIDE_PACKET_NAME] = packetCreator?.createInt32(NUM_HANDS)!!
        processor?.setInputSidePackets(inputNumHands)
    }

    private fun startCamera() {
        Log.w(TAG, "startCamera.")

        lifecycleRegistry?.markState(Lifecycle.State.STARTED)

        cameraHelper = CameraXPreviewHelper()
        cameraHelper?.setOnCameraStartedListener { texture ->
            Log.w(TAG, "startCamera.setOnCameraStartedListener.")
            surfaceTexture = texture!!
            surfaceView?.visibility = View.VISIBLE
        }
        cameraHelper!!.startCamera(this, CameraHelper.CameraFacing.FRONT, null)
    }


    companion object {

        const val TAG = ">>>>> AirMouse >>>>>"

        const val BINARY_GRAPH_NAME = "hand_tracking_mobile_gpu.binarypb"
        const val INPUT_STREAM_NAME = "input_video"
        const val OUTPUT_STREAM_NAME = "output_video"
        const val OUTPUT_LANDMARKS_STREAM_NAME = "hand_landmarks"

        const val INPUT_NUM_HANDS_SIDE_PACKET_NAME = "num_hands"
        const val NUM_HANDS = 1

        const val ACTION_SHOW = "com.jk.asmouse.airmouse.show"
        const val ACTION_HIDE = "com.jk.asmouse.airmouse.hide"
    }
}